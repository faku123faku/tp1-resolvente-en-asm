
extern printf

section .data
puntero dq 25.0, 10.0, 5.0, 23.0, 3.0

ARR_SIZE equ ($ - puntero)/8

r dq 2.0

aux_count dq 0

 
msg db "%f ", 10, 13, 0


section .text

global main
main:
    mov ebp, esp; for correct debugging
    
    ;se utiliza ebx para desplazarse
    ;por el vector cada 8 posiciones (DQ)
    mov ebx, 0
    
    ;ecx indica la cantidad de iteraciones de .ciclo
    mov ecx, ARR_SIZE
    
    .ciclo:
        
        ;cargo r y uno de los valores del vector en el stack
        fld qword [r]
        fld qword [puntero+ebx]
        
        ;realizo el producto escalar
        fmul
        
        ;reemplazo el valor en la posicion actual del vector por el nuevo
        fst qword [puntero+ebx]
        
        ;guardo el valor de ecx para que no se pierda por
        ;el printf
        mov [aux_count], ecx
        
        ;imprimo el resultado
        push dword [puntero+ebx+4]
        push dword [puntero+ebx]
        push msg
        
        ;imprimo el nuevo valor de la posicion en curso del vector
        call printf
        
        add esp, 12
    
        ;restauro el contador en ecx
        mov ecx, [aux_count]
        
        ;incremento el valor de desplazamiento del vector
        add ebx, 8
        
        ;decremento el contador para iterar
        sub ecx, 1
        
        jnz .ciclo
    
    ret