# TP1 - Resolvente en ASM

Trabajo práctico N°1 para Organización del Computador 2021

## Indice

* [Introducción](#introducción)
* [Requisitos](#requisitos)
* [Instalación](#instalación)
    * [Formula Resolvente](#formula-resolvente)
    * [Producto Escalar de un vector](#producto-escalar-de-un-vector)
* [Demo](#demo)
    * [Formula Resolvente Demo](#formula-resolvente-demo)
    * [Producto Escalar Demo](#producto-escalar-demo)
* [Profesores](#profesores)
* [Autor](#autor)

## Introducción
En el siguiente escrito, se redacta la resolución de las actividades del TP1, los requisitos y comandos necesarios para compilar y ejecutar los programas, y una demostración de los programas en ejecución.

## Requisitos
- SO Ubuntu 16.04
- GCC v5.4.0
- NASM v2.11.08

## Instalación
Para poder compilar y ejecutar los programas de las actividades se debe ejecutar los siguientes comandos desde la carpeta root.

### Formula Resolvente
Para compilar el programa de la formula resolvente, ejecutar:

```bash
cd resolvente
nasm -f elf32 resolvente-asm.s -o resolvente-asm.o
gcc -m32 -o ejecutable resolvente-asm.o resolvente-params.c
```

Una vez compilado el programa, utilizar el siguiente comando para ejecutarlo:

```bash
./ejecutable
```

Para compilar y ejecutar automáticamente se puede ejecutar el archivo *.sh* de la siguiente manera:

```bash
cd resolvente
sh resolvente-sh.sh
```

**Funcionamiento**

El programa está constituido por dos archivos. Por un lado, el archivo resolvente-params.c se encarga de recibir los tres coeficientes de punto flotante de una función cuadrática por consola y luego llama a las funciones *resolvente_suma* y *resolvente_resta* desarrolladas en NASM para obtener las raíces posibles e imprimirlas por consola.

Ambas funciones desarrolladas en NASM siguen la misma lógica para recibir y almacenar los parámetros. La pila recibe los parámetros desde el archivo .C en el orden *Funcion, A, B, C* y aquellos valores de punto flotante son almacenados en la pila FPU para realizar las operaciones necesarias para aplicar la fórmula resolvente de una función cuadrática. Finalmente, se almacena la raíz obtenida según la función ejecutada. 

La diferencia entre las funciones *resolvente_suma* y *resolvente_resta* es que la primera realiza la suma de -b + rcuadrada( b^2 - 4ac) y la otra realiza la resta -b - rcuadrada( b^2 - 4ac).

**Problemas Encontrados**

- **Valor de retorno:** En un principio no se comprendió como devolver el valor final de la función realizada en NASM a C. Sin embargo, después de comprobar la documentacion y realizar pruebas el resultado final es devuelto en el registro EAX.

- **Resultado NaN:** Al probar el programa con parámetros en el que el resultado de b^2 - 4ac sea menor a 0 o que haya una división por 0, las funciones de NASM devolvían como resultado NaN, ocasionando un problema en el archivo .C para imprimir por consola. Se pudo resolver colocando restricciones a la hora de solicitar los coeficientes desde el archivo .C.

### Producto Escalar de un vector
Para compilar el programa del cálculo del producto escalar de un vector, ejecutar:

```bash
cd producto-escalar
nasm -f elf32 vector-asm.s
gcc -m32 vector-asm.o -o producto-escalar
```

Una vez compilado el programa, utilizar el siguiente comando para ejecutarlo:

```bash
./producto-escalar
```

Para compilar y ejecutar automáticamente se puede ejecutar el archivo *.sh* de la siguiente manera:

```bash
cd producto-escalar
sh producto-escalar-sh.sh
```

**Funcionamiento**

El programa está formado por un único archivo NASM, en el cual se encuentra toda la lógica para calcular el producto escalar de un vector con valores de punto flotante.

Para empezar, se declara los valores que tendrá el vector en la variable *puntero* y el valor con el cual se calculará el producto escalar en la variable *r*. Una vez definidas las variables, el programa utilizará el registro *EBX* para desplazarse en el vector y el registro *ECX* como contador para finalizar el desplazamiento en el vector.

Después, el programa entrará en la etiqueta *.ciclo* para recorrer el vector. En primer lugar, se guarda el valor que haya en la posicion del vector en curso y el de *r* en la FPU Stack. En segundo lugar, se realiza la multiplicación entre los dos primeros valores almacenados en la FPU Stack y el resultado es reemplazado en la posicion del vector en curso. En tercer lugar, se almacena el valor del registro *ECX* en una variable auxiliar para evitar perder el valor del registro a causa de la llamada a la subrutina **PRINTF**. Finalmente, se imprime por consola el resultado del producto entre *r* y el valor de la posición en curso del vector y se restauran el registro *ECX*, se incrementa el valor de *EBX* en 8 para desplazarse al siguiente valor del vector y se decrementa *ECX* para finalizar el ciclo cuando éste llegue a cero.

**Problemas Encontrados**

- **Ciclo Infinito:** Al iterar sobre el vector se detectó que el ciclo nunca terminaba, por lo que el programa no podia ejecutarse correctamente. El problema lo ocasionó la llamada a la subrutina **PRINTF** porque modificaba el valor del registro *ECX* utilizado como contador para detener el ciclo. La solución fue utilizar una variable auxiliar para salvar el valor del registro *ECX* y restaurarlo despues de llamar a la subrutina.

- **Creación de un nuevo vector:** No se pudo encontrar la forma de crear una nueva variable de tipo vector de manera dinámica y asignarle los resultados del producto escalar. Como solución se decidió modificar los valores del vector existente y disponerlo como el vector resultado a imprimir.

## Demo
### Formula Resolvente Demo
Una vez ejecutado el comando para correr el programa de la fórmula resolvente, se solicitará por consola los valores de A, B y C de la fórmula cuadrática para obtener las raíces

**Aclaración:** *Formula cuadrática = ax^2 + bx + c*

![](images/resolvente-ok-1.png)

Una vez ingresado los 3 valores, el programa devolverá por consola las raíces posibles de la función cuadrática evaluada.

![](images/resolvente-ok-2.png)

El programa tiene como limitación que la operación b^2 - 4ac debe ser mayor o igual a 0, en caso de no cumplirla devolverá un mensaje de advertencia.

![](images/resolvente-bad-2.png)

Tampoco se admiten valores menores a 0 para el parámetro A

![](images/resolvente-bad-1.png)

### Producto Escalar Demo
Una vez ejecutado el comando para correr el programa de producto escalar, el programa calculará por defecto el producto escalar del vector 25.0, 10.0, 5.0, 23.0, 3.0 multiplicado por 2.0.

![](images/producto-escalar-result.png)

Para cambiar el valor del vector y el valor para multiplicar, se puede acceder al código fuente del archivo vector-asm.s y modificar las variables *puntero* y *r*.

![](images/producto-escalar-code.png)

El producto escalar se guarda en la misma variable *puntero* para imprimirlo por consola.

## Profesores
- Alles Sergio Gastón - sgalles@campus.ungs.edu.ar
- Dominguez Ariel - adominguez@campus.ungs.edu.ar

## Autor
- Avila Facundo Mauricio - DNI 39506986 - faku1234faku@gmail.com.




