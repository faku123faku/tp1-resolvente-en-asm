#include <stdio.h>

extern float resolvente_suma(float a, float b, float c);
extern float resolvente_resta(float a, float b, float c);

int main() {
    float valorA;
    float valorB;
    float valorC;

    printf("Ingrese el valor de A:");
    scanf("%f", &valorA);

    printf("Ingrese el valor de B:");
    scanf("%f", &valorB);

    printf("Ingrese el valor de C:");
    scanf("%f", &valorC);

    float restaEnLaRaiz = valorB * valorB - 4 * valorA * valorC;

    if(restaEnLaRaiz < 0 ) {
        printf("El resultado de b^2 - 4ac debe ser mayor o igual a 0.\n");
    } else if (valorA <= 0) {
        printf("El valor de a debe ser mayor a 0.\n");
    } else {

        float resolventeSuma = resolvente_suma( valorA, valorB, valorC);

        float resolventeResta = resolvente_resta( valorA, valorB, valorC);
        
        printf("Las raices son: %f y %f\n", resolventeSuma, resolventeResta);
    }

    return 0;
}