

section .data

constante_4 dd 4.0
constante_2 dd 2.0

resultado dd 0.0

section .text
global resolvente_suma
global resolvente_resta

resolvente_suma:

    push EBP
    
    mov EBP, ESP
    
    ; variable -b
    fld dword [EBP + 12]
    fchs
    
    ; b^2
    fld dword [EBP + 12]
    fmul st0, st0
    
    ; 4ac
    fld dword [constante_4]
    fld dword [EBP + 8]
    fmul
    fld dword [EBP + 16]
    fmul
    
    ; b^2 - 4ac
    fsub
    
    ; raiz cuadrada de b^2 - 4ac
    fsqrt
    
    ; -b + raiz cuadrada de b^2 - 4ac
    fadd
    
    fld dword [constante_2]
    fld dword [EBP + 8]
    fmul
    
    fdiv
    
    ;almacenar el tope de pila en resultado
    fst qword [resultado]
    
    ;valor de retorno
    mov eax, [resultado]
    
    ;leave
    mov EBP, ESP
    pop EBP
    
    ret
    
resolvente_resta:

    push EBP
    
    mov EBP, ESP
    
    ; variable -b
    fld dword [EBP + 12]
    fchs
    
    ; b^2
    fld dword [EBP + 12]
    fmul st0, st0
    
    ; 4ac
    fld dword [constante_4]
    fld dword [EBP + 8]
    fmul
    fld dword [EBP + 16]
    fmul
    
    ; b^2 - 4ac
    fsub
    
    ; raiz cuadrada de b^2 - 4ac
    fsqrt
    
    ; -b - raiz cuadrada de b^2 - 4ac
    fsub
    
    fld dword [constante_2]
    fld dword [EBP + 8]
    fmul
    
    fdiv
    
    ;almacenar el tope de pila en resultado
    fst qword [resultado]
    
    ;valor de retorno
    mov eax, [resultado]
    
    ;leave
    mov EBP, ESP
    pop EBP
    
    ret